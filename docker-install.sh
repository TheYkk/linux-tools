#!/bin/bash -eux

## INSTALL docker

# add certificates Docker
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -




add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get -y update


apt-get install -y docker-ce

service docker stop
service docker start

# handle permissions
usermod -aG docker $1